declare const menuItems: {
    items: ({
        id: string;
        title: string;
        type: string;
        children: {
            id: string;
            title: string;
            type: string;
            url: string;
            icon: import("@tabler/icons").TablerIcon;
            breadcrumbs: boolean;
        }[];
    } | {
        id: string;
        title: string;
        type: string;
        children: ({
            id: string;
            title: string;
            type: string;
            icon: import("@tabler/icons").TablerIcon;
            children: {
                id: string;
                title: string;
                type: string;
                url: string;
                icon: any;
                breadcrumbs: boolean;
            }[];
            url?: undefined;
        } | {
            id: string;
            title: string;
            type: string;
            url: string;
            icon: import("@tabler/icons").TablerIcon;
            children?: undefined;
        })[];
    } | {
        id: string;
        type: string;
        children: {
            id: string;
            title: string;
            type: string;
            url: string;
            icon: import("@tabler/icons").TablerIcon;
            external: boolean;
            target: boolean;
        }[];
    })[];
};
export default menuItems;
