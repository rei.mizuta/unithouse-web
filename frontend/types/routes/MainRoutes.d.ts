/// <reference types="react" />
declare const MainRoutes: {
    path: string;
    element: JSX.Element;
    children: {
        path: string;
        element: JSX.Element;
    }[];
};
export default MainRoutes;
