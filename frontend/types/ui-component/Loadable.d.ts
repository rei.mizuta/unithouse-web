/// <reference types="react" />
declare const Loadable: (Component: any) => (props: any) => JSX.Element;
export default Loadable;
