/// <reference types="react" />
/**
 * if you want to use image instead of <svg> uncomment following.
 *
 * import logoDark from 'assets/images/logo-dark.svg';
 * import logo from 'assets/images/logo.svg';
 *
 */
declare const Logo: () => JSX.Element;
export default Logo;
