/**
 * Represent theme style and structure as per Material-UI
 * @param {JsonObject} customization customization parameter object
 */
export declare const theme: (customization: any) => import("@mui/material/styles").Theme;
export default theme;
