export declare const SET_MENU = "@customization/SET_MENU";
export declare const MENU_TOGGLE = "@customization/MENU_TOGGLE";
export declare const MENU_OPEN = "@customization/MENU_OPEN";
export declare const SET_FONT_FAMILY = "@customization/SET_FONT_FAMILY";
export declare const SET_BORDER_RADIUS = "@customization/SET_BORDER_RADIUS";
