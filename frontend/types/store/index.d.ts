declare const store: import("redux").Store<import("redux").EmptyObject & {
    customization: {
        opened: any;
        isOpen: any[];
        fontFamily: any;
        borderRadius: any;
    };
}, import("redux").AnyAction>;
declare const persister = "Free";
export { store, persister };
