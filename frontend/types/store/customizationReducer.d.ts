export declare const initialState: {
    isOpen: any[];
    fontFamily: any;
    borderRadius: any;
    opened: boolean;
};
declare const customizationReducer: (state: {
    isOpen: any[];
    fontFamily: any;
    borderRadius: any;
    opened: boolean;
}, action: any) => {
    opened: any;
    isOpen: any[];
    fontFamily: any;
    borderRadius: any;
};
export default customizationReducer;
