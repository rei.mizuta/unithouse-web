declare const reducer: import("redux").Reducer<import("redux").CombinedState<{
    customization: {
        opened: any;
        isOpen: any[];
        fontFamily: any;
        borderRadius: any;
    };
}>, import("redux").AnyAction>;
export default reducer;
