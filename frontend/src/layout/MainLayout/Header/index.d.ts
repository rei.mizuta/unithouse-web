/// <reference types="react" />
declare const Header: {
    ({ handleLeftDrawerToggle }: {
        handleLeftDrawerToggle: any;
    }): JSX.Element;
    propTypes: {
        handleLeftDrawerToggle: any;
    };
};
export default Header;
