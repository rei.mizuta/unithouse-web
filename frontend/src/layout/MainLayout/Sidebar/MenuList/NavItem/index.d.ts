/// <reference types="react" />
declare const NavItem: {
    ({ item, level }: {
        item: any;
        level: any;
    }): JSX.Element;
    propTypes: {
        item: any;
        level: any;
    };
};
export default NavItem;
