/// <reference types="react" />
declare const NavGroup: {
    ({ item }: {
        item: any;
    }): JSX.Element;
    propTypes: {
        item: any;
    };
};
export default NavGroup;
