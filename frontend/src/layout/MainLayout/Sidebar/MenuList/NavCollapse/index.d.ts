/// <reference types="react" />
declare const NavCollapse: {
    ({ menu, level }: {
        menu: any;
        level: any;
    }): JSX.Element;
    propTypes: {
        menu: any;
        level: any;
    };
};
export default NavCollapse;
