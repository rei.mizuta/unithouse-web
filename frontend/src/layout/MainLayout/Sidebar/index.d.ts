/// <reference types="react" />
declare const Sidebar: {
    ({ drawerOpen, drawerToggle, window }: {
        drawerOpen: any;
        drawerToggle: any;
        window: any;
    }): JSX.Element;
    propTypes: {
        drawerOpen: any;
        drawerToggle: any;
        window: any;
    };
};
export default Sidebar;
