import { useSelector } from 'react-redux';

import { ThemeProvider } from '@mui/material/styles';
import { CssBaseline, StyledEngineProvider } from '@mui/material';

import reducer from 'store/reducer';
// routing
import Routes from 'routes';

// defaultTheme
import themes from 'themes';

// project imports
import NavigationScroll from 'layout/NavigationScroll';

// ==============================|| APP ||============================== //
declare interface AppState {
    customization:
    | {
        opened: any;
        isOpen: any[];
        fontFamily: string;
        borderRadius: number;
    }
    | {
        fontFamily: any;
        isOpen: any[];
        borderRadius: number;
        opened: boolean;
    }
    | {
        borderRadius: any;
        isOpen: any[];
        fontFamily: string;
        opened: boolean;
    };
}

const App = () => {
    const customization = useSelector((state: AppState) => state.customization);

    return (
        <StyledEngineProvider injectFirst>
            <ThemeProvider theme={themes(customization)}>
                <CssBaseline />
                <NavigationScroll>
                    <Routes />
                </NavigationScroll>
            </ThemeProvider>
        </StyledEngineProvider>
    );
};

export default App;
