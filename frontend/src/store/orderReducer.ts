// action - state management
import * as actionTypes from "./actions";

export const HouseSize = { Four: "4坪", Five: "5坪" };
export type HouseSize = typeof HouseSize[keyof typeof HouseSize];
export const OrderType = { Rental: "レンタル", Sell: "販売" };
export type OrderType = typeof OrderType[keyof typeof OrderType];
export const AssignStatus = { NotFixed: "未確定", Fixed: "確定" };
export type AssignStatus = typeof AssignStatus[keyof typeof AssignStatus];
export const OrderStatus = {
  NotSent: "未出庫",
  Sent: "出庫中",
  Received: "返却済",
};
export type OrderStatus = typeof OrderStatus[keyof typeof OrderStatus];

export type HouseMetadata = {
  rentousu: number;
  size: HouseSize;
  kaisu: number;
  setNumber: number;
};

export type HouseType = "A" | "B" | "C";
export type AssignableHouseType = HouseType | null;
export type HouseCode = string;
export type AssignableHouseCode = HouseCode | null;
export type Position = "a" | "b" | "c" | "d";
export type KaidanPosition =
  | "ar"
  | "br"
  | "cr"
  | "dr"
  | "al"
  | "bl"
  | "cl"
  | "dl";

export type HouseAssign = {
  type: AssignableHouseType;
  code: AssignableHouseCode;
};

export type HouseSetRequirement = {
  OneFloor: HouseAssign[];
  TwoFloor: HouseAssign[];
  majikiriOneFloor: number[];
  majikiriTwoFloor: number[];
  kaidan: KaidanPosition[][];
  odoriba: Position[][];
};

export type HouseRequirement = {
  metadata: HouseMetadata;
  content: HouseSetRequirement[];
};

export type OrderMetadata = {
  size: HouseSize;
  type: OrderType;
  assignStatus: AssignStatus;
  orderKey: string;
  sendDate: Date;
  sendActualDate?: Date;
  receiveDate?: Date;
  receiveActualDate?: Date;
  companyName: string;
  gemba: string;
  orderStatus: OrderStatus;
};

export type OrderEntry = OrderMetadata & {
  houseRequirement: HouseRequirement;
};

export type Order = OrderEntry & { id: number };

const initialState: { orders: Order[] } = {
  orders: [
    {
      id: 1,
      size: HouseSize.Five,
      type: OrderType.Rental,
      assignStatus: AssignStatus.Fixed,
      orderKey: "10000",
      sendDate: new Date(2022, 0, 1),
      sendActualDate: new Date(2022, 0, 1),
      receiveDate: new Date(2022, 5, 1),
      companyName: "A会社",
      gemba: "A現場",
      orderStatus: OrderStatus.NotSent,
      houseRequirement: {
        metadata: { rentousu: 4, kaisu: 2, setNumber: 1, size: HouseSize.Five },
        content: [
          {
            OneFloor: [
              { type: "A", code: "5UHJ-0000" },
              { type: "B", code: "5UHJ-0001" },
              { type: "B", code: "5UHJ-0002" },
              { type: "C", code: "5UHJ-0003" },
            ],
            TwoFloor: [
              { type: "A", code: "5UHJ-0004" },
              { type: "B", code: "5UHJ-0005" },
              { type: "B", code: "5UHJ-0006" },
              { type: "C", code: "5UHJ-0007" },
            ],
            majikiriOneFloor: [2, 5],
            majikiriTwoFloor: [3, 6],
            kaidan: [[], [], [], ["dl"]],
            odoriba: [[], [], [], ["d"]],
          },
        ],
      },
    },
  ],
};

// ==============================|| ORDER REDUCER ||============================== //

export const orderReducer = (
  state: { orders: Order[] } = initialState,
  action
) => {
  switch (action.type) {
    case actionTypes.CREATE_ORDER:
      const orderEntry = {
        ...action.order,
        id: Math.max(...state.orders.map((item) => item.id)) + 1,
      }; // TODO: replace mock
      return { orders: [...state.orders, orderEntry] };
    case actionTypes.UPDATE_ORDER:
      return {
        orders: [
          ...state.orders.filter((item) => item.id !== action.order.id),
          { ...action.order },
        ],
      };
    case actionTypes.DELETE_ORDER:
      return {
        orders: [...state.orders.filter((item) => item.id !== action.order.id)],
      };
    default:
      return state;
  }
};
