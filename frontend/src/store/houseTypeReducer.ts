import { HouseSize } from "./orderReducer";
// action - state management
import * as actionTypes from "./actions";

export type Attachments = {
  kitchen: boolean;
  window: boolean;
  entrance: boolean;
  wc: boolean;
  battery: boolean;
};

export type HouseTypeEntry = {
  size: HouseSize;
  name: string;
  attachments: { a: Attachments; d: Attachments }; // TODO: support side
  createDate: Date;
};

export type HouseType = HouseTypeEntry & { id: number };

const initialState: { houseTypes: HouseType[] } = {
  houseTypes: [
    {
      id: 1,
      size: HouseSize.Four,
      name: "A",
      attachments: {
        a: {
          kitchen: true,
          window: false,
          entrance: false,
          wc: false,
          battery: false,
        },
        d: {
          kitchen: false,
          window: true,
          entrance: false,
          wc: false,
          battery: false,
        },
      },
      createDate: new Date(2021, 0, 1),
    },
    {
      id: 2,
      size: HouseSize.Four,
      name: "B",
      attachments: {
        a: {
          kitchen: false,
          window: false,
          entrance: false,
          wc: false,
          battery: false,
        },
        d: {
          kitchen: false,
          window: true,
          entrance: false,
          wc: false,
          battery: false,
        },
      },
      createDate: new Date(2021, 0, 1),
    },
    {
      id: 3,
      size: HouseSize.Four,
      name: "C",
      attachments: {
        a: {
          kitchen: false,
          window: false,
          entrance: false,
          wc: true,
          battery: false,
        },
        d: {
          kitchen: false,
          window: false,
          entrance: false,
          wc: false,
          battery: false,
        },
      },
      createDate: new Date(2021, 0, 1),
    },
  ],
};

// ==============================|| ORDER REDUCER ||============================== //

export const houseTypeReducer = (
  state: { houseTypes: HouseType[] } = initialState,
  action: any
) => {
  switch (action.type) {
    case actionTypes.CREATE_HOUSETYPE:
      const houseTypeEntry = {
        ...action.houseType,
        id: Math.max(...state.houseTypes.map((item) => item.id)) + 1,
      }; // TODO: replace mock
      return { houseTypes: [...state.houseTypes, houseTypeEntry] };
    case actionTypes.UPDATE_HOUSETYPE:
      return {
        houseTypes: [
          ...state.houseTypes.filter((item) => item.id !== action.houseType.id),
          { ...action.houseType },
        ],
      };
    case actionTypes.DELETE_HOUSETYPE:
      return {
        houseTypes: [
          ...state.houseTypes.filter((item) => item.id !== action.houseType.id),
        ],
      };
    default:
      return state;
  }
};
