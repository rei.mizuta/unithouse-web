import { combineReducers } from "redux";

// reducer import
import customizationReducer from "./customizationReducer";
import { orderReducer } from "./orderReducer";
import { houseItemReducer } from "./houseItemReducer";
import { houseTypeReducer } from "./houseTypeReducer";

// ==============================|| COMBINE REDUCER ||============================== //

const reducer = combineReducers({
  customization: customizationReducer,
  order: orderReducer,
  houseItem: houseItemReducer,
  houseType: houseTypeReducer,
});

export default reducer;
