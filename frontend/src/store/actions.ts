// action - customization reducer
export const SET_MENU = "@customization/SET_MENU";
export const MENU_TOGGLE = "@customization/MENU_TOGGLE";
export const MENU_OPEN = "@customization/MENU_OPEN";
export const SET_FONT_FAMILY = "@customization/SET_FONT_FAMILY";
export const SET_BORDER_RADIUS = "@customization/SET_BORDER_RADIUS";

// action - order reducer
export const CREATE_ORDER = "@customization/CREATE_ORDER";
export const UPDATE_ORDER = "@customization/UPDATE_ORDER";
export const DELETE_ORDER = "@customization/DELETE_ORDER";

// action - houseitem reducer
export const CREATE_HOUSEITEM = "@customization/CREATE_HOUSEITEM";
export const UPDATE_HOUSEITEM = "@customization/UPDATE_HOUSEITEM";
export const DELETE_HOUSEITEM = "@customization/DELETE_HOUSEITEM";

// action - housetype reducer
export const CREATE_HOUSETYPE = "@customization/CREATE_HOUSETYPE";
export const UPDATE_HOUSETYPE = "@customization/UPDATE_HOUSETYPE";
export const DELETE_HOUSETYPE = "@customization/DELETE_HOUSETYPE";
