import { HouseSize, HouseType } from "./orderReducer";
// action - state management
import * as actionTypes from "./actions";

export const HouseItemStatus = {
  Creating: "製造中",
  Sent: "出庫中",
  Sold: "販売済",
  Abondoned: "廃棄済",
  Maintain: "整備中",
  Ready: "整備済",
};
export type HouseItemStatus =
  typeof HouseItemStatus[keyof typeof HouseItemStatus];

export type HouseItemEntry = {
  size: HouseSize;
  name: string;
  type: HouseType;
  status: HouseItemStatus;
  createDate: Date;
};

export type HouseItem = HouseItemEntry & { id: number };

const initialState: { houseItems: HouseItem[] } = {
  houseItems: [
    {
      id: 1,
      size: HouseSize.Four,
      name: "1000",
      type: "A",
      status: HouseItemStatus.Ready,
      createDate: new Date(2021, 0, 1),
    },
    {
      id: 2,
      size: HouseSize.Four,
      name: "1001",
      type: "A",
      status: HouseItemStatus.Ready,
      createDate: new Date(2021, 0, 1),
    },
    {
      id: 3,
      size: HouseSize.Four,
      name: "1002",
      type: "A",
      status: HouseItemStatus.Ready,
      createDate: new Date(2021, 0, 1),
    },
  ],
};

// ==============================|| ORDER REDUCER ||============================== //

export const houseItemReducer = (
  state: { houseItems: HouseItem[] } = initialState,
  action: any
) => {
  switch (action.type) {
    case actionTypes.CREATE_HOUSEITEM:
      const houseItemEntry = {
        ...action.houseItem,
        id: Math.max(...state.houseItems.map((item) => item.id)) + 1,
      }; // TODO: replace mock
      return { houseItems: [...state.houseItems, houseItemEntry] };
    case actionTypes.UPDATE_HOUSEITEM:
      return {
        houseItems: [
          ...state.houseItems.filter((item) => item.id !== action.houseItem.id),
          { ...action.houseItem },
        ],
      };
    case actionTypes.DELETE_HOUSEITEM:
      return {
        houseItems: [
          ...state.houseItems.filter((item) => item.id !== action.houseItem.id),
        ],
      };
    default:
      return state;
  }
};
