declare const dashboard: {
    id: string;
    title: string;
    type: string;
    children: {
        id: string;
        title: string;
        type: string;
        url: string;
        icon: import("@tabler/icons").TablerIcon;
        breadcrumbs: boolean;
    }[];
};
export default dashboard;
