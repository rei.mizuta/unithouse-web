declare const pages: {
    id: string;
    title: string;
    type: string;
    children: ({
        id: string;
        title: string;
        type: string;
        icon: import("@tabler/icons").TablerIcon;
        children: {
            id: string;
            title: string;
            type: string;
            url: string;
            icon: any;
            breadcrumbs: boolean;
        }[];
        url?: undefined;
    } | {
        id: string;
        title: string;
        type: string;
        url: string;
        icon: import("@tabler/icons").TablerIcon;
        children?: undefined;
    })[];
};
export default pages;
