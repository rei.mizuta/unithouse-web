// assets
import { IconKey } from "@tabler/icons";

// constant
const icons = {
  IconKey,
};

// ==============================|| EXTRA PAGES MENU ITEMS ||============================== //

const pages = {
  id: "pages",
  title: "Pages",
  type: "group",
  children: [
    {
      id: "orders",
      title: "予定・実績入力",
      type: "collapse",
      icon: icons.IconKey,
      children: [
        {
          id: "size4",
          title: "4坪",
          type: "item",
          url: "/order/size4",
          icon: icons.IconDashboard,
          breadcrumbs: false,
        },
        {
          id: "size5",
          title: "5坪",
          type: "item",
          url: "/order/size5",
          icon: icons.IconDashboard,
          breadcrumbs: false,
        },
      ],
    },
    {
      id: "house-items",
      title: "製品番号一覧",
      type: "item",
      url: "/house-items",
      icon: icons.IconKey,
    },
    {
      id: "house-types",
      title: "型番登録",
      type: "item",
      url: "/house-types",
      icon: icons.IconKey,
    },
  ],
};

export default pages;
