declare const other: {
    id: string;
    type: string;
    children: {
        id: string;
        title: string;
        type: string;
        url: string;
        icon: import("@tabler/icons").TablerIcon;
        external: boolean;
        target: boolean;
    }[];
};
export default other;
