/// <reference types="react" />
declare const useScriptRef: () => import("react").MutableRefObject<boolean>;
export default useScriptRef;
