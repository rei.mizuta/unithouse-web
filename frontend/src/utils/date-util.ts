export const dateToString = (
  date: Date | string | undefined | null,
  delim: "-" | "/" = "-"
): string => {
  if (!date) return "";
  return new Date(date).toISOString().slice(0, 10).replaceAll("-", delim);
};
