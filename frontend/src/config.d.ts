declare const config: {
    basename: string;
    defaultPath: string;
    fontFamily: string;
    borderRadius: number;
};
export default config;
