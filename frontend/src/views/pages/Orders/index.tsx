import { DataGrid, GridRenderCellParams } from "@mui/x-data-grid";
import { Button } from "@mui/material";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { HouseSize } from "store/orderReducer";
import Attributes from "ui-component/checkbox/Attributes";

const columns = [
  {
    field: "sendDate",
    headerName: "出庫予定日",
    width: 130,
    valueGetter: ({ value }: { value?: Date }) =>
      value && new Date(value).toISOString().slice(0, 10).replaceAll("-", "/"),
  }, // TODO:avoid kludge
  {
    field: "receiveDate",
    headerName: "入庫予定日",
    width: 130,
    valueGetter: ({ value }: { value?: Date }) =>
      value && new Date(value).toISOString().slice(0, 10).replaceAll("-", "/"),
  },
  { field: "companyName", headerName: "会社名", width: 130 },
  { field: "gemba", headerName: "現場名", width: 130 },
  {
    field: "orderStatus",
    headerName: "ステータス",
    width: 130,
    renderCell: (params: GridRenderCellParams<string>) => (
      <Link to={`update/${params.row.id}`} style={{ textDecoration: "none" }}>
        <Button
          variant="contained"
          color="primary"
          size="small"
          style={{ marginLeft: 16 }}
        >
          {params.row.orderStatus}
        </Button>
      </Link>
    ),
  },
];

const OrderView: React.FC<{ size: HouseSize }> = ({ size }) => {
  const order = useSelector((state) => state.order);
  const handleCellClick = (param, event) => {
    event.stopPropagation();
  };
  return (
    <div style={{ height: "80%", width: "100%" }}>
      <Link to="create" style={{ textDecoration: "none" }}>
        <Button variant="contained" color="primary">
          新規受注を登録
        </Button>
      </Link>
      <DataGrid
        rows={order.orders.filter((item) => item.size === size)}
        columns={columns}
        onCellClick={handleCellClick}
        checkboxSelection
      />
      <Link to="create" style={{ textDecoration: "none" }}>
        <Button variant="contained" color="primary">
          新規受注を登録
        </Button>
      </Link>
    </div>
  );
};

export default OrderView;
