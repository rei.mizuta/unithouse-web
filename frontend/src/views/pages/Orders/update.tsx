import {
  Box,
  Paper,
  Button,
  Grid,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import { saveAs } from "file-saver";
import GetAppIcon from "@mui/icons-material/GetApp";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { UPDATE_ORDER } from "store/actions";
import {
  HouseRequirement,
  Order,
  OrderEntry,
  OrderMetadata,
  OrderStatus,
  OrderType,
} from "store/orderReducer";
import DemoImage from "assets/images/demo/【建具表付】セット 1.png";
import OrderMetadataInput from "ui-component/house/OrderMetadataInput";
import OrderNumeralInput from "ui-component/house/OrderNumeralInput";
import HouseInput from "ui-component/house/HouseInput";

const OrderCreateSuggestHouseItem = () => {
  return (
    <>
      <Button variant="contained" color="primary">
        割り当て
      </Button>
    </>
  );
};

export default function OrderUpdateView() {
  const { id } = useParams();
  const order: Order = useSelector((state: any) => state.order).orders.filter(
    (item: Order) => item.id === parseInt(id || "0") //TODO: avoid kludge
  )[0];
  const [houseRequirement, updateHouseRequirement] = useState<HouseRequirement>(
    { ...order.houseRequirement }
  );
  const [orderMetadata, updateOrderMetadata] = useState<OrderMetadata>({
    ...order,
  });

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const updateOrder = () => {
    dispatch({
      type: UPDATE_ORDER,
      order: { ...orderMetadata, houseRequirement: houseRequirement },
    });
    navigate(-1);
  };
  return (
    <div style={{ width: "100%" }}>
      <OrderMetadataInput
        orderMetadata={orderMetadata}
        updateOrderMetadata={updateOrderMetadata}
        create={false}
      />
      <Grid>
        <OrderNumeralInput
          houseRequirement={houseRequirement}
          updateHouseRequirement={updateHouseRequirement}
        />
      </Grid>
      <HouseInput
        houseRequirement={houseRequirement}
        updateHouseRequirement={updateHouseRequirement}
      />
      <Button variant="contained" color="primary">
        製品番号を自動割り当て
      </Button>
      <Button variant="contained" color="primary" onClick={updateOrder}>
        変更
      </Button>
      <OrderCreateSuggestHouseItem />
    </div>
  );
}
