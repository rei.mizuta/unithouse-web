import {
  Button,
  TextField,
  Paper,
  Grid,
  Select,
  MenuItem,
  Box,
  InputLabel,
  FormControl,
  Typography,
} from "@mui/material";
import { GridRow } from "@mui/x-data-grid";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { CREATE_ORDER } from "store/actions";
import {
  HouseType,
  AssignStatus,
  OrderType,
  OrderStatus,
  HouseSize,
  OrderMetadata,
  HouseRequirement,
  KaidanPosition,
} from "store/orderReducer";
import Attributes from "ui-component/checkbox/Attributes";
import AttributesCounter from "ui-component/checkbox/AttributesCounter";
import HouseInput from "ui-component/house/HouseInput";
import OrderMetadataInput from "ui-component/house/OrderMetadataInput";
import OrderNumeralInput from "ui-component/house/OrderNumeralInput";
import { dateToString } from "utils/date-util";

const OrderCreateSuggestHouseItem = () => {
  return (
    <>
      <Button variant="contained" color="success">
        確定
      </Button>
    </>
  );
};

const OrderCreateView: React.FC<{ size: HouseSize }> = ({ size }) => {
  const navigate = useNavigate();
  const [orderMetadata, updateOrderMetadata] = useState<OrderMetadata>({
    size: size,
    type: OrderType.Rental,
    assignStatus: AssignStatus.NotFixed,
    orderKey: "",
    sendDate: new Date(),
    companyName: "",
    gemba: "",
    orderStatus: OrderStatus.NotSent,
  });
  const [houseRequirement, updateHouseRequirement] = useState<HouseRequirement>(
    {
      metadata: { rentousu: 2, kaisu: 1, setNumber: 1, size: size },
      content: [],
    }
  );
  const dispatch = useDispatch();

  const createOrder = () => {
    dispatch({
      type: CREATE_ORDER,
      order: { ...orderMetadata, houseRequirement: houseRequirement },
    });
    navigate(-1);
  };

  const [houseRequirementMock, updateHouseRequirementMock] =
    useState<HouseRequirement>({
      metadata: { rentousu: 3, kaisu: 2, setNumber: 1, size: size },
      content: [
        {
          OneFloor: [
            { type: "A", code: "1000" },
            { type: "B", code: "1001" },
            { type: "C", code: "1002" },
          ],
          TwoFloor: [
            { type: "A", code: "1003" },
            { type: "B", code: "1004" },
            { type: "C", code: "1005" },
          ],
          majikiriOneFloor: [],
          majikiriTwoFloor: [],
          kaidan: [["dr"], [], []],
          odoriba: [["d"], ["d"], ["d"]],
        },
      ],
      /*metadata: { rentousu: 8, kaisu: 2, setNumber: 1, size: size },
      content: [
        {
          OneFloor: [
            { type: "A", code: "1000" },
            { type: "B", code: "1001" },
            { type: "B", code: "1002" },
            { type: "B", code: "1003" },
            { type: "B", code: "1004" },
            { type: "B", code: "1005" },
            { type: "B", code: "1006" },
            { type: "C", code: "1007" },
          ],
          TwoFloor: [
            { type: "A", code: "1008" },
            { type: "B", code: "1009" },
            { type: "B", code: "1010" },
            { type: "B", code: "1011" },
            { type: "B", code: "1012" },
            { type: "B", code: "1013" },
            { type: "B", code: "1014" },
            { type: "C", code: "1015" },
          ],
          majikiriOneFloor: [],
          majikiriTwoFloor: [],
          kaidan: [["dr"], [], [], [], [], [], [], []],
          odoriba: [["d"], ["d"], ["d"], ["d"], ["d"], ["d"], ["d"], ["d"]],
        },
      ],*/
    });
  const [mockSuggest, updateMockSuggest] = useState(false);

  return (
    <div style={{ width: "100%" }}>
      <OrderMetadataInput
        orderMetadata={orderMetadata}
        updateOrderMetadata={updateOrderMetadata}
        create
      />
      <Grid>
        <OrderNumeralInput
          houseRequirement={houseRequirement}
          updateHouseRequirement={updateHouseRequirement}
        />
      </Grid>
      <HouseInput
        houseRequirement={houseRequirement}
        updateHouseRequirement={updateHouseRequirement}
      />
      <Button variant="contained" color="primary" onClick={createOrder}>
        確定せず保存
      </Button>
      <OrderCreateSuggestHouseItem />
      <Grid sx={{ pt: 5 }} container rowSpacing={2}>
        <Grid item lg={12}>
          <Typography variant="h3">付属品個数の指定</Typography>
        </Grid>
        <Grid item lg={12}>
          <AttributesCounter />
        </Grid>
        <Grid item lg={12}>
          <Button
            variant="contained"
            color="primary"
            onClick={() => updateMockSuggest(true)}
          >
            製品番号割り当ての提案
          </Button>
        </Grid>
        {mockSuggest && (
          <>
            <Grid item lg={12}>
              <Button
                variant="contained"
                color="primary"
                onClick={() =>
                  updateHouseRequirement({ ...houseRequirementMock })
                }
              >
                提案された割り当てを反映
              </Button>
            </Grid>

            <Grid item lg={12} md={12}>
              <Grid container style={{ backgroundColor: "lightGreen" }}>
                <HouseInput
                  houseRequirement={houseRequirementMock}
                  updateHouseRequirement={updateHouseRequirementMock}
                />
              </Grid>
            </Grid>
          </>
        )}
      </Grid>
    </div>
  );
};

export default OrderCreateView;
