import { Button, MenuItem, Select, TextField } from "@mui/material";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { CREATE_HOUSEITEM } from "store/actions";
import { HouseItemEntry, HouseItemStatus } from "store/houseItemReducer";
import { HouseSize } from "store/orderReducer";

const HouseItemCreateInput: React.FC<any> = ({
  houseItem,
  updateHouseItem,
}) => {
  const handleUpdate = (event: any) => {
    updateHouseItem({ ...houseItem, [event.target.id]: event.target.value });
  };
  const houseType = useSelector((state: any) => state.houseType);
  return (
    <>
      <h1>新棟製造</h1>
      <Select
        id="size"
        label="坪数"
        onChange={handleUpdate}
        value={houseItem.size}
      >
        <MenuItem value={HouseSize.Four}>{HouseSize.Four}</MenuItem>
        <MenuItem value={HouseSize.Five}>{HouseSize.Five}</MenuItem>
      </Select>
      <TextField
        id="name"
        label="新規製品番号"
        value={houseItem.name}
        onChange={handleUpdate}
      />
      <Select
        id="type"
        label="型番名"
        onChange={(e) =>
          updateHouseItem({ ...houseItem, type: e.target.value })
        }
      >
        <MenuItem disabled value="">
          <em>型番名</em>
        </MenuItem>
        {houseType.houseTypes.map((item) => (
          <MenuItem value={item.name}>{item.name}</MenuItem>
        ))}
      </Select>
      <TextField
        id="createDate"
        type="date"
        value={houseItem.createDate}
        label="製造予定日"
        InputLabelProps={{
          shrink: true,
        }}
        onChange={handleUpdate}
      />
    </>
  );
};

export default function HouseItemCreateView() {
  const navigate = useNavigate();
  const [houseItem, updateHouseItem] = useState<HouseItemEntry>({
    name: "",
    size: HouseSize.Four,
    type: "A",
    status: HouseItemStatus.Creating,
    createDate: new Date(),
  });
  const dispatch = useDispatch();

  const createHouseItem = () => {
    dispatch({ type: CREATE_HOUSEITEM, houseItem: { ...houseItem } });
    navigate(-1);
  };
  return (
    <div style={{ width: "100%" }}>
      <HouseItemCreateInput
        houseItem={houseItem}
        updateHouseItem={updateHouseItem}
      />
      <Button variant="contained" color="primary" onClick={createHouseItem}>
        新規登録
      </Button>
    </div>
  );
}
