import { Button } from "@mui/material";
import { DataGrid, GridRenderCellParams } from "@mui/x-data-grid";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { HouseItemStatus } from "store/houseItemReducer";

const columns = [
  { field: "name", headerName: "製品番号", width: 200 },
  { field: "type", headerName: "棟タイプ", width: 130 },
  {
    field: "status",
    headerName: "ステータス",
    width: 130,
    renderCell: (params: GridRenderCellParams<HouseItemStatus>) => (
      <Link to={`update/${params.row.id}`} style={{ textDecoration: "none" }}>
        <Button
          variant="contained"
          color="primary"
          size="small"
          style={{ marginLeft: 16 }}
        >
          {params.row.status}
        </Button>
      </Link>
    ),
  },
];

export default function HouseItemView() {
  const houseItem = useSelector((state) => state.houseItem);
  return (
    <div style={{ height: 400, width: "100%" }}>
      <Link to="create" style={{ textDecoration: "none" }}>
        <Button variant="contained" color="primary">
          新棟製造
        </Button>
      </Link>
      <DataGrid rows={houseItem.houseItems} columns={columns} />
      <Link to="create" style={{ textDecoration: "none" }}>
        <Button variant="contained" color="primary">
          新棟製造
        </Button>
      </Link>
    </div>
  );
}
