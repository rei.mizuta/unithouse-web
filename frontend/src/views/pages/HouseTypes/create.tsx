import {
  Button,
  Grid,
  MenuItem,
  Paper,
  Box,
  Select,
  TextField,
} from "@mui/material";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import { CREATE_HOUSETYPE } from "store/actions";
import { HouseTypeEntry } from "store/houseTypeReducer";
import { HouseSize } from "store/orderReducer";
import Attributes from "ui-component/checkbox/Attributes";

const HouseTypeCreateInput: React.FC<any> = ({
  houseType,
  updateHouseType,
}) => {
  const handleUpdate = (event: any) => {
    updateHouseType({ ...houseType, [event.target.id]: event.target.value });
  };
  return (
    <>
      <h1>型番登録</h1>
      <Select
        id="size"
        label="坪数"
        onChange={handleUpdate}
        value={houseType.size}
      >
        <MenuItem value={HouseSize.Four}>{HouseSize.Four}</MenuItem>
        <MenuItem value={HouseSize.Five}>{HouseSize.Five}</MenuItem>
      </Select>

      <TextField
        id="name"
        label="棟タイプ名"
        value={houseType.type}
        onChange={handleUpdate}
      />
      <Grid container justifyContent="center">
        <Grid item>
          <Attributes
            initialValue={houseType.attachments.a}
            updateValue={(value) =>
              updateHouseType({
                ...houseType,
                attachments: { ...houseType.attachments, a: { ...value } },
              })
            }
          />
        </Grid>
      </Grid>
      <Grid container justifyContent="center">
        <Grid item>
          <Paper sx={{ height: 200, width: 100 }} square variant="outlined" />
        </Grid>
      </Grid>
      <Grid container justifyContent="center">
        <Grid item>
          <Attributes
            initialValue={houseType.attachments.d}
            updateValue={(value) =>
              updateHouseType({
                ...houseType,
                attachments: { ...houseType.attachments, d: { ...value } },
              })
            }
          />
        </Grid>
      </Grid>
    </>
  );
};

export default function HouseTypeCreateView() {
  const navigate = useNavigate();
  const [houseType, updateHouseType] = useState<HouseTypeEntry>({
    name: "",
    size: HouseSize.Four,
    attachments: {
      a: {
        kitchen: false,
        window: false,
        entrance: false,
        wc: false,
        battery: false,
      },
      d: {
        kitchen: false,
        window: false,
        entrance: false,
        wc: false,
        battery: false,
      },
    },
    createDate: new Date(),
  });
  const dispatch = useDispatch();

  const createHouseType = () => {
    dispatch({ type: CREATE_HOUSETYPE, houseType: { ...houseType } });
    navigate(-1);
  };
  return (
    <div style={{ width: "100%" }}>
      <HouseTypeCreateInput
        houseType={houseType}
        updateHouseType={updateHouseType}
      />
      <Button variant="contained" color="primary" onClick={createHouseType}>
        新規登録
      </Button>
    </div>
  );
}
