import { DataGrid } from "@mui/x-data-grid";
import { Button, MenuItem, Select, TextField } from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { UPDATE_HOUSEITEM } from "store/actions";
import { HouseSize } from "store/orderReducer";
import { useState } from "react";
import { HouseItem, HouseItemStatus } from "store/houseItemReducer";

const HouseItemUpdateInput: React.FC<any> = ({
  houseItem,
  updateHouseItem,
}) => {
  const handleUpdate = (event: any) => {
    updateHouseItem({ ...houseItem, [event.target.id]: event.target.value });
  };
  return (
    <>
      <h1>製品番号 {houseItem.name}</h1>
      <Select
        id="size"
        label="坪数"
        onChange={handleUpdate}
        value={houseItem.size}
      >
        <MenuItem value={HouseSize.Four}>{HouseSize.Four}</MenuItem>
        <MenuItem value={HouseSize.Five}>{HouseSize.Five}</MenuItem>
      </Select>
      <TextField
        id="name"
        label="新規製品番号"
        value={houseItem.name}
        onChange={handleUpdate}
      />
      <TextField
        id="type"
        label="棟タイプ"
        value={houseItem.type}
        onChange={handleUpdate}
      />
      <TextField
        id="createDate"
        type="date"
        value={houseItem.createDate}
        label="製造予定日"
        InputLabelProps={{
          shrink: true,
        }}
        onChange={handleUpdate}
      />
      <TextField
        id="status"
        label="ステータス"
        onChange={(event) =>
          updateHouseItem({ ...houseItem, status: event.target.value })
        }
        style={{ width: 150 }}
        select
      >
        {Object.values(HouseItemStatus).map((value) => (
          <MenuItem value={value}>{value}</MenuItem>
        ))}
      </TextField>
    </>
  );
};

export default function HouseItemUpdateView() {
  const navigate = useNavigate();
  const { id } = useParams();
  const houseItemStore = useSelector(
    (state) => state.houseItem
  ).houseItems.filter((item) => item.id == id)[0];

  const [houseItem, updateHouseItem] = useState<HouseItem>({
    ...houseItemStore,
  });
  const dispatch = useDispatch();

  const updateHouseItemDispatch = () => {
    dispatch({ type: UPDATE_HOUSEITEM, houseItem: { ...houseItem } });
    navigate(-1);
  };
  return (
    <div style={{ height: 400, width: "100%" }}>
      <HouseItemUpdateInput
        houseItem={houseItem}
        updateHouseItem={updateHouseItem}
      />
      <Button
        variant="contained"
        color="primary"
        onClick={updateHouseItemDispatch}
      >
        更新
      </Button>
    </div>
  );
}
