import { Button } from "@mui/material";
import { DataGrid, GridRenderCellParams } from "@mui/x-data-grid";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { HouseItemStatus } from "store/houseItemReducer";

const columns = [
  { field: "name", headerName: "棟タイプ", width: 130 },
  {
    field: "status",
    headerName: "ステータス",
    width: 130,
    renderCell: (params: GridRenderCellParams<HouseItemStatus>) => (
      <Link to={`update/${params.row.id}`} style={{ textDecoration: "none" }}>
        <Button
          variant="contained"
          color="primary"
          size="small"
          style={{ marginLeft: 16 }}
        >
          型番仕様
        </Button>
      </Link>
    ),
  },
];

export default function HouseTypeView() {
  const houseItem = useSelector((state) => state.houseType);
  return (
    <div style={{ height: 400, width: "100%" }}>
      <Link to="create" style={{ textDecoration: "none" }}>
        <Button variant="contained" color="primary">
          新しい型番の登録
        </Button>
      </Link>
      <DataGrid rows={houseItem.houseTypes} columns={columns} />
      <Link to="create" style={{ textDecoration: "none" }}>
        <Button variant="contained" color="primary">
          新しい型番の登録
        </Button>
      </Link>
    </div>
  );
}
