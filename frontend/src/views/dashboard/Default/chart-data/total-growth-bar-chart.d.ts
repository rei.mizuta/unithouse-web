declare const chartData: {
    height: number;
    type: string;
    options: {
        chart: {
            id: string;
            stacked: boolean;
            toolbar: {
                show: boolean;
            };
            zoom: {
                enabled: boolean;
            };
        };
        responsive: {
            breakpoint: number;
            options: {
                legend: {
                    position: string;
                    offsetX: number;
                    offsetY: number;
                };
            };
        }[];
        plotOptions: {
            bar: {
                horizontal: boolean;
                columnWidth: string;
            };
        };
        xaxis: {
            type: string;
            categories: string[];
        };
        legend: {
            show: boolean;
            fontSize: string;
            fontFamily: string;
            position: string;
            offsetX: number;
            labels: {
                useSeriesColors: boolean;
            };
            markers: {
                width: number;
                height: number;
                radius: number;
            };
            itemMargin: {
                horizontal: number;
                vertical: number;
            };
        };
        fill: {
            type: string;
        };
        dataLabels: {
            enabled: boolean;
        };
        grid: {
            show: boolean;
        };
    };
    series: {
        name: string;
        data: number[];
    }[];
};
export default chartData;
