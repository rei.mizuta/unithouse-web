declare const chartData: {
    type: string;
    height: number;
    options: {
        chart: {
            id: string;
            sparkline: {
                enabled: boolean;
            };
        };
        dataLabels: {
            enabled: boolean;
        };
        stroke: {
            curve: string;
            width: number;
        };
        tooltip: {
            fixed: {
                enabled: boolean;
            };
            x: {
                show: boolean;
            };
            y: {
                title: string;
            };
            marker: {
                show: boolean;
            };
        };
    };
    series: {
        data: number[];
    }[];
};
export default chartData;
