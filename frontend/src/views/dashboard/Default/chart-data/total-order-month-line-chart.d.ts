declare const chartData: {
    type: string;
    height: number;
    options: {
        chart: {
            sparkline: {
                enabled: boolean;
            };
        };
        dataLabels: {
            enabled: boolean;
        };
        colors: string[];
        fill: {
            type: string;
            opacity: number;
        };
        stroke: {
            curve: string;
            width: number;
        };
        yaxis: {
            min: number;
            max: number;
        };
        tooltip: {
            theme: string;
            fixed: {
                enabled: boolean;
            };
            x: {
                show: boolean;
            };
            y: {
                title: string;
            };
            marker: {
                show: boolean;
            };
        };
    };
    series: {
        name: string;
        data: number[];
    }[];
};
export default chartData;
