/// <reference types="react" />
declare const TotalOrderLineChartCard: {
    ({ isLoading }: {
        isLoading: any;
    }): JSX.Element;
    propTypes: {
        isLoading: any;
    };
};
export default TotalOrderLineChartCard;
