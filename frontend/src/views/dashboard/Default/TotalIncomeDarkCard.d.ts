/// <reference types="react" />
declare const TotalIncomeDarkCard: {
    ({ isLoading }: {
        isLoading: any;
    }): JSX.Element;
    propTypes: {
        isLoading: any;
    };
};
export default TotalIncomeDarkCard;
