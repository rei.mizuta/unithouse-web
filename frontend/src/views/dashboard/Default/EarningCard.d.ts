/// <reference types="react" />
declare const EarningCard: {
    ({ isLoading }: {
        isLoading: any;
    }): JSX.Element;
    propTypes: {
        isLoading: any;
    };
};
export default EarningCard;
