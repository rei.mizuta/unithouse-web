/// <reference types="react" />
declare const PopularCard: {
    ({ isLoading }: {
        isLoading: any;
    }): JSX.Element;
    propTypes: {
        isLoading: any;
    };
};
export default PopularCard;
