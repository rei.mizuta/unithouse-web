/// <reference types="react" />
declare const TotalGrowthBarChart: {
    ({ isLoading }: {
        isLoading: any;
    }): JSX.Element;
    propTypes: {
        isLoading: any;
    };
};
export default TotalGrowthBarChart;
