/// <reference types="react" />
declare const TotalIncomeLightCard: {
    ({ isLoading }: {
        isLoading: any;
    }): JSX.Element;
    propTypes: {
        isLoading: any;
    };
};
export default TotalIncomeLightCard;
