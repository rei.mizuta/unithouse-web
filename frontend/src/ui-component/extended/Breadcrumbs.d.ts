/// <reference types="react" />
declare const Breadcrumbs: {
    ({ card, divider, icon, icons, maxItems, navigation, rightAlign, separator, title, titleBottom, ...others }: {
        [x: string]: any;
        card: any;
        divider: any;
        icon: any;
        icons: any;
        maxItems: any;
        navigation: any;
        rightAlign: any;
        separator: any;
        title: any;
        titleBottom: any;
    }): JSX.Element;
    propTypes: {
        card: any;
        divider: any;
        icon: any;
        icons: any;
        maxItems: any;
        navigation: any;
        rightAlign: any;
        separator: any;
        title: any;
        titleBottom: any;
    };
};
export default Breadcrumbs;
