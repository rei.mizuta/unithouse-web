/// <reference types="react" />
declare const Avatar: {
    ({ color, outline, size, sx, ...others }: {
        [x: string]: any;
        color: any;
        outline: any;
        size: any;
        sx: any;
    }): JSX.Element;
    propTypes: {
        className: any;
        color: any;
        outline: any;
        size: any;
        sx: any;
    };
};
export default Avatar;
