/// <reference types="react" />
declare const ImagePlaceholder: ({ ...others }: {
    [x: string]: any;
}) => JSX.Element;
export default ImagePlaceholder;
