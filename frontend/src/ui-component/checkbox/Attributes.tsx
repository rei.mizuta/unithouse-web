import * as React from "react";
import Checkbox from "@mui/material/Checkbox";

import CountertopsIcon from "@mui/icons-material/Countertops";
import WindowIcon from "@mui/icons-material/Window";
import MeetingRoomIcon from "@mui/icons-material/MeetingRoom";
import WcIcon from "@mui/icons-material/Wc";
import BoltIcon from "@mui/icons-material/Bolt";
import { Grid, Tooltip } from "@mui/material";

const label = { inputProps: { "aria-label": "Checkbox" } };

const CheckboxIcon = ({
  tooltip,
  element,
  updateValue,
  checked = false,
  readOnly = false,
}) => {
  return (
    <Tooltip title={tooltip}>
      <Checkbox
        {...label}
        icon={element}
        onChange={(e) => updateValue(e.target.value)}
        checkedIcon={element}
        inputProps={{
          readOnly: readOnly,
        }}
        defaultChecked={checked}
      />
    </Tooltip>
  );
};

export default function Attributes({
  initialValue,
  updateValue,
  editable = true,
}) {
  const update = (name: string) => (bl: boolean) => {
    updateValue({ ...initialValue, [name]: bl });
  };
  return (
    <div>
      <Grid container>
        <CheckboxIcon
          tooltip="キッチン"
          element={<CountertopsIcon />}
          updateValue={update("kitchen")}
          checked={initialValue.kitchen}
          readOnly={!editable}
        />
        <CheckboxIcon
          tooltip="窓"
          element={<WindowIcon />}
          updateValue={update("window")}
          checked={initialValue.window}
          readOnly={!editable}
        />
        <CheckboxIcon
          tooltip="出入口"
          element={<MeetingRoomIcon />}
          updateValue={update("entrance")}
          checked={initialValue.entrance}
          readOnly={!editable}
        />
      </Grid>
      <Grid container>
        <CheckboxIcon
          tooltip="トイレ"
          element={<WcIcon />}
          updateValue={update("wc")}
          checked={initialValue.wc}
          readOnly={!editable}
        />
        <CheckboxIcon
          tooltip="AC電源"
          element={<BoltIcon />}
          updateValue={update("battery")}
          checked={initialValue.battery}
          readOnly={!editable}
        />
      </Grid>
    </div>
  );
}
