import * as React from "react";
import Checkbox from "@mui/material/Checkbox";

import CountertopsIcon from "@mui/icons-material/Countertops";
import WindowIcon from "@mui/icons-material/Window";
import MeetingRoomIcon from "@mui/icons-material/MeetingRoom";
import WcIcon from "@mui/icons-material/Wc";
import BoltIcon from "@mui/icons-material/Bolt";
import SingleBedIcon from "@mui/icons-material/SingleBed";
import AccountBoxIcon from "@mui/icons-material/AccountBox";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import { Grid, TextField, Tooltip, Typography } from "@mui/material";
import { IconSmoking } from "@tabler/icons";

const label = { inputProps: { "aria-label": "Checkbox" } };

const CounterWithIcon = ({ name, description, element }) => {
  return (
    <>
      {element}
      <TextField
        id={name}
        type="number"
        inputProps={{ inputMode: "numeric", pattern: "[0-9]*" }}
        style={{ width: 80 }}
      />
      <Typography variant="h4">{description}</Typography>
    </>
  );
};

export default function AttributesCounter() {
  return (
    <div>
      <Grid container>
        <Grid item lg={3}>
          <CounterWithIcon
            name="kitchen"
            description="キッチン"
            element={<CountertopsIcon />}
          />
        </Grid>
        <Grid item lg={3}>
          <CounterWithIcon
            name="window"
            description="窓"
            element={<WindowIcon />}
          />
        </Grid>
        <Grid item lg={3}>
          <CounterWithIcon
            name="entrance"
            description="出入口"
            element={<MeetingRoomIcon />}
          />
        </Grid>
        <Grid item lg={3}>
          <CounterWithIcon
            name="wc"
            description="トイレ"
            element={<WcIcon />}
          />
        </Grid>
        <Grid item lg={3}>
          <CounterWithIcon
            name="battery"
            description="AC電源"
            element={<BoltIcon />}
          />
        </Grid>

        <Grid item lg={3}>
          <CounterWithIcon
            name="smoking"
            description="喫煙室"
            element={<IconSmoking />}
          />
        </Grid>
        <Grid item lg={3}>
          <CounterWithIcon
            name="breakroom"
            description="休憩室"
            element={<SingleBedIcon />}
          />
        </Grid>

        <Grid item lg={3}>
          <CounterWithIcon
            name="population"
            description="人数"
            element={<AccountBoxIcon />}
          />
        </Grid>

        <Grid item lg={3}>
          <CounterWithIcon
            name="duration"
            description="工期（ヶ月）"
            element={<AccessTimeIcon />}
          />
        </Grid>
      </Grid>
    </div>
  );
}
