import { Button, Grid, MenuItem, TextField } from "@mui/material";
import { Dispatch, SetStateAction, SyntheticEvent } from "react";
import { OrderMetadata, OrderStatus, OrderType } from "store/orderReducer";
import GetAppIcon from "@mui/icons-material/GetApp";
import { saveAs } from "file-saver";
import DemoImage from "assets/images/demo/【建具表付】セット 1.png";
import { dateToString } from "utils/date-util";

const OrderMetadataInput: React.FC<{
  orderMetadata: OrderMetadata;
  updateOrderMetadata: Dispatch<SetStateAction<OrderMetadata>>;
  create: boolean;
}> = ({ orderMetadata, updateOrderMetadata, create = false }) => {
  const handleUpdate = (event: React.ChangeEvent<HTMLInputElement>) => {
    updateOrderMetadata({
      ...orderMetadata,
      [event.target.id]:
        event.target.type === "date"
          ? new Date(event.target.value)
          : event.target.value,
    });
  };
  return (
    <>
      <h1>
        {orderMetadata.size} 受注 {create ? "登録" : "照会"}
      </h1>
      <Grid>
        <Button
          variant="contained"
          startIcon={<GetAppIcon />}
          onClick={() => {
            saveAs(DemoImage, "平面図.png");
          }}
        >
          平面図をダウンロード
        </Button>
      </Grid>
      <TextField
        id="type"
        label="レンタル/販売"
        onChange={(event) =>
          updateOrderMetadata({ ...orderMetadata, type: event.target.value })
        }
        style={{ width: 150 }}
        value={orderMetadata.type}
        select
      >
        <MenuItem value={OrderType.Rental}>{OrderType.Rental}</MenuItem>
        <MenuItem value={OrderType.Sell}>{OrderType.Sell}</MenuItem>
      </TextField>
      <TextField id="orderKey" label="受注No." onChange={handleUpdate} />
      <TextField
        id="sendDate"
        label="出庫予定日"
        InputLabelProps={{
          shrink: true,
        }}
        value={dateToString(orderMetadata.sendDate)}
        type="date"
        onChange={handleUpdate}
      />
      {orderMetadata.type === OrderType.Rental ? (
        <TextField
          id="receiveDate"
          label="入庫予定日"
          InputLabelProps={{
            shrink: true,
          }}
          value={dateToString(orderMetadata.receiveDate)}
          type="date"
          onChange={handleUpdate}
        />
      ) : (
        <></>
      )}
      <Grid>
        <TextField
          id="companyName"
          label="企業名"
          value={orderMetadata.companyName}
          onChange={handleUpdate}
        />
        <TextField
          id="gemba"
          label="現場名"
          value={orderMetadata.gemba}
          onChange={handleUpdate}
        />
      </Grid>
      <Grid>
        <TextField
          id="type"
          label="ステータス"
          onChange={(event) =>
            updateOrderMetadata({
              ...orderMetadata,
              orderStatus: event.target.value,
            })
          }
          value={orderMetadata.orderStatus}
          style={{ width: 150 }}
          select
        >
          <MenuItem value={OrderStatus.NotSent}>{OrderStatus.NotSent}</MenuItem>
          <MenuItem value={OrderStatus.Sent}>{OrderStatus.Sent}</MenuItem>
          <MenuItem value={OrderStatus.Received}>
            {OrderStatus.Received}
          </MenuItem>
        </TextField>
        {orderMetadata.orderStatus === OrderStatus.Sent && (
          <TextField
            id="sendActualDate"
            label="出庫実績日"
            InputLabelProps={{
              shrink: true,
            }}
            value={orderMetadata.sendActualDate}
            type="date"
            onChange={handleUpdate}
          />
        )}
        {orderMetadata.orderStatus === OrderStatus.Received && (
          <>
            <TextField
              id="sendActualDate"
              label="出庫実績日"
              InputLabelProps={{
                shrink: true,
              }}
              value={orderMetadata.sendActualDate}
              type="date"
              onChange={handleUpdate}
            />
            <TextField
              id="receiveActualDate"
              label="入庫実績日"
              InputLabelProps={{
                shrink: true,
              }}
              value={orderMetadata.receiveActualDate}
              type="date"
              onChange={handleUpdate}
            />
          </>
        )}
      </Grid>
    </>
  );
};

export default OrderMetadataInput;
