import {
  Grid,
  MenuItem,
  Paper,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import { ReactNode, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { HouseItem } from "store/houseItemReducer";
import { HouseType } from "store/houseTypeReducer";
import {
  HouseMetadata,
  HouseSetRequirement,
  KaidanPosition,
} from "store/orderReducer";
import Attributes from "ui-component/checkbox/Attributes";

const replaceAt = <T,>(arr: T[], index: number, value: T): T[] => {
  if (arr.length <= index) return arr;
  let arr_ret: T[] = [];
  arr.forEach((elem, idx) => {
    if (idx == index) arr_ret.push(value);
    else arr_ret.push(elem);
  });
  return arr_ret;
};

const HousePieceInput: React.FC<{
  houseMetadata: HouseMetadata;
  setIdx: number;
  kaisu: number;
  index: number;
  content: HouseSetRequirement[];
  updateContent: (content: HouseSetRequirement) => void;
}> = ({ houseMetadata, setIdx, kaisu, index, content, updateContent }) => {
  const [odoribaInitial, kaidanInitial, houseItemCodes] = [
    content[setIdx].odoriba,
    content[setIdx].kaidan,
    content[setIdx][kaisu ? "TwoFloor" : "OneFloor"],
  ];

  const majikiriArray =
    kaisu === 1
      ? content[setIdx].majikiriOneFloor
      : content[setIdx].majikiriTwoFloor;

  const majikiriCenter = majikiriArray.includes(2 * index + 1);
  const updateMajikiriCenter = (bool: boolean) => {
    if (kaisu === 1) {
      updateContent({
        ...content[setIdx],
        majikiriOneFloor: bool
          ? [...majikiriArray, 2 * index + 1]
          : majikiriArray.filter((num) => num !== 2 * index + 1),
      });
    } else {
      updateContent({
        ...content[setIdx],
        majikiriTwoFloor: bool
          ? [...majikiriArray, 2 * index + 1]
          : majikiriArray.filter((num) => num !== 2 * index + 1),
      });
    }
  };

  const majikiriRight = majikiriArray.includes(2 * index + 2);
  const updateMajikiriRight = (bool: boolean) => {
    if (kaisu === 1) {
      updateContent({
        ...content[setIdx],
        majikiriOneFloor: bool
          ? [...majikiriArray, 2 * index + 2]
          : majikiriArray.filter((num) => num !== 2 * index + 2),
      });
    } else {
      updateContent({
        ...content[setIdx],
        majikiriTwoFloor: bool
          ? [...majikiriArray, 2 * index + 2]
          : majikiriArray.filter((num) => num !== 2 * index + 2),
      });
    }
  };
  const [odoribaUp, updateOdoribaUp] = [
    odoribaInitial[index].includes("a"),
    (bool: boolean) =>
      updateContent({
        ...content[setIdx],
        odoriba: bool
          ? replaceAt(content[setIdx].odoriba, index, [
              ...odoribaInitial[index],
              "a",
            ])
          : replaceAt(
              content[setIdx].odoriba,
              index,
              odoribaInitial[index].filter((item) => item !== "a")
            ),
      }),
  ];

  const [odoribaLeftUp, updateOdoribaLeftUp] = [
    !index && odoribaInitial[index].includes("b"),
    (bool: boolean) =>
      updateContent({
        ...content[setIdx],
        odoriba: bool
          ? replaceAt(content[setIdx].odoriba, index, [
              ...odoribaInitial[index],
              "b",
            ])
          : replaceAt(
              content[setIdx].odoriba,
              index,
              odoribaInitial[index].filter((item) => item !== "b")
            ),
      }),
  ];
  const [odoribaLeftBottom, updateOdoribaLeftBottom] = [
    !index && odoribaInitial[index].includes("c"),
    (bool: boolean) =>
      updateContent({
        ...content[setIdx],
        odoriba: bool
          ? replaceAt(content[setIdx].odoriba, index, [
              ...odoribaInitial[index],
              "c",
            ])
          : replaceAt(
              content[setIdx].odoriba,
              index,
              odoribaInitial[index].filter((item) => item !== "c")
            ),
      }),
  ];
  const [odoribaRightUp, updateOdoribaRightUp] = [
    index == houseMetadata.rentousu - 1 && odoribaInitial[index].includes("b"),
    (bool: boolean) =>
      updateContent({
        ...content[setIdx],
        odoriba: bool
          ? replaceAt(content[setIdx].odoriba, index, [
              ...odoribaInitial[index],
              "b",
            ])
          : replaceAt(
              content[setIdx].odoriba,
              index,
              odoribaInitial[index].filter((item) => item !== "b")
            ),
      }),
  ];

  const [odoribaRightBottom, updateOdoribaRightBottom] = [
    index == houseMetadata.rentousu - 1 && odoribaInitial[index].includes("c"),
    (bool: boolean) =>
      updateContent({
        ...content[setIdx],
        odoriba: bool
          ? replaceAt(content[setIdx].odoriba, index, [
              ...odoribaInitial[index],
              "c",
            ])
          : replaceAt(
              content[setIdx].odoriba,
              index,
              odoribaInitial[index].filter((item) => item !== "c")
            ),
      }),
  ];
  const [odoribaBottom, updateOdoribaBottom] = [
    odoribaInitial[index].includes("d"),
    (bool: boolean) =>
      updateContent({
        ...content[setIdx],
        odoriba: bool
          ? replaceAt(content[setIdx].odoriba, index, [
              ...odoribaInitial[index],
              "d",
            ])
          : replaceAt(
              content[setIdx].odoriba,
              index,
              odoribaInitial[index].filter((item) => item !== "d")
            ),
      }),
  ];

  const [kaidan, updateKaidan] = [
    {
      al: kaidanInitial[index].includes("al"),
      ar: kaidanInitial[index].includes("ar"),
      bl: kaidanInitial[index].includes("bl"),
      br: kaidanInitial[index].includes("br"),
      cl: kaidanInitial[index].includes("cl"),
      cr: kaidanInitial[index].includes("cr"),
      dl: kaidanInitial[index].includes("dl"),
      dr: kaidanInitial[index].includes("dr"),
    },
    (newKaidan: { [kp in KaidanPosition]: boolean }) => {
      updateContent({
        ...content[setIdx],
        kaidan: replaceAt(
          kaidanInitial,
          index,
          Object.keys(newKaidan).filter((key) => newKaidan[key])
        ),
      });
    },
  ];

  useEffect(() => {
    if (!odoribaUp) updateKaidan({ ...kaidan, al: false, ar: false });
  }, [odoribaUp]);
  useEffect(() => {
    if (!odoribaBottom) updateKaidan({ ...kaidan, dl: false, dr: false });
  }, [odoribaBottom]);
  useEffect(() => {
    if (!(odoribaLeftUp || odoribaRightUp))
      updateKaidan({ ...kaidan, bl: false, br: false });
  }, [odoribaLeftUp, odoribaRightUp]);
  useEffect(() => {
    if (!(odoribaLeftBottom || odoribaRightBottom))
      updateKaidan({ ...kaidan, cl: false, cr: false });
  }, [odoribaLeftBottom, odoribaRightBottom]);

  const LengthData = {
    Width: 130,
    Height: 170,
    OdoribaHeight: 50,
    MajikiriWidth: 20,
  };

  const majikiriMouseOver = (event: any) => {
    event.target.style.background = "black";
  };
  const majikiriMouseLeave = (already: boolean) => (event: any) => {
    if (!already) event.target.style.background = null;
  };
  const odoribaMouseOver = (event: any) => {
    event.target.style.background = "white";
    event.target.style.border = "1px solid black";
    event.target.innerText = "踊り場";
  };
  const odoribaMouseLeave = (already: boolean) => (event: any) => {
    if (!already) {
      event.target.style.background = null;
      event.target.style.border = null;
      event.target.innerText = "";
    }
  };
  const kaidanMouseOver = (event: any) => {
    event.target.style.background = "yellow";
    event.target.style.border = "1px solid black";
    event.target.innerText = "階段";
  };
  const kaidanMouseLeave = (already: boolean) => (event: any) => {
    if (!already) {
      event.target.style.background = null;
      event.target.style.border = null;
      event.target.innerText = "";
    }
  };

  const houseItem: { houseItems: HouseItem[] } = useSelector(
    (state: any) => state.houseItem
  );
  const houseType: { houseTypes: HouseType[] } = useSelector(
    (state: any) => state.houseType
  );
  /*const tabIndexOffset =
          houseMetadata.kaisu === 1
              ? (houseMetadata.rentousu * 2 - 1) * setIdx + 2 * index
              : (houseMetadata.rentousu * 2 - 1) * (2 * setIdx + (2 - kaisu)) + 2 * index;*/
  return (
    <>
      <Paper
        sx={{
          position: "relative",
          height: LengthData.Height,
          width: LengthData.Width,
        }}
        square
        variant="outlined"
      >
        {/* misc input */}
        <div
          style={{
            position: "absolute",
            left: 0,
            top: -LengthData.OdoribaHeight * 3,
            zIndex: 100,
            textAlign: "center",
            lineHeight: "3rem",
          }}
          role="checkbox"
          aria-checked="false"
          aria-label="odoriba up input"
          tabIndex={0}
        >
          {houseItemCodes[index].type ? (
            <Attributes
              initialValue={
                houseType.houseTypes.filter(
                  (item) => item.name == houseItemCodes[index].type
                )[0].attachments.a
              }
              updateValue={() => {}}
              editable={false}
            />
          ) : (
            <Attributes
              initialValue={{
                kitchen: false,
                window: false,
                entrance: false,
                wc: false,
                battery: false,
              }}
              updateValue={() => {}}
              editable={true}
            />
          )}
        </div>
        <div
          style={{
            position: "absolute",
            left: 0,
            top: LengthData.Height + LengthData.OdoribaHeight,
            zIndex: 100,
            textAlign: "center",
            lineHeight: "3rem",
          }}
          role="checkbox"
          aria-checked="false"
          aria-label="odoriba up input"
          tabIndex={0}
        >
          {houseItemCodes[index].type ? (
            <Attributes
              initialValue={
                houseType.houseTypes.filter(
                  (item) => item.name == houseItemCodes[index].type
                )[0].attachments.d
              }
              updateValue={() => {}}
              editable={false}
            />
          ) : (
            <Attributes
              initialValue={{
                kitchen: false,
                window: false,
                entrance: false,
                wc: false,
                battery: false,
              }}
              updateValue={() => {}}
              editable={true}
            />
          )}
        </div>
        {/* kaidan input */}
        {odoribaUp && !kaidan.al && (
          <div
            style={{
              position: "absolute",
              height: LengthData.OdoribaHeight,
              width: LengthData.Width / 3,
              left: 0,
              top: -LengthData.OdoribaHeight,
              zIndex: 200,
              textAlign: "center",
              lineHeight: "3rem",
              backgroundColor: undefined,
              border: undefined,
            }}
            onMouseOver={kaidanMouseOver}
            onFocus={kaidanMouseOver}
            onMouseLeave={kaidanMouseLeave(kaidan.al)}
            onKeyDown={() => {}}
            onClick={() => {
              kaidan.al
                ? updateKaidan({ ...kaidan, al: false })
                : updateKaidan({ ...kaidan, al: true });
            }}
            role="checkbox"
            aria-checked="false"
            aria-label="kaidan al input"
            tabIndex={0}
          />
        )}
        {odoribaUp && !kaidan.ar && (
          <div
            style={{
              position: "absolute",
              height: LengthData.OdoribaHeight,
              width: LengthData.Width / 3,
              left: 0 + (LengthData.Width * 2) / 3,
              top: -LengthData.OdoribaHeight,
              zIndex: 200,
              textAlign: "center",
              lineHeight: "3rem",
              backgroundColor: undefined,
              border: undefined,
            }}
            onMouseOver={kaidanMouseOver}
            onFocus={kaidanMouseOver}
            onMouseLeave={kaidanMouseLeave(kaidan.ar)}
            onKeyDown={() => {}}
            onClick={() => {
              kaidan.ar
                ? updateKaidan({ ...kaidan, ar: false })
                : updateKaidan({ ...kaidan, ar: true });
            }}
            role="checkbox"
            aria-checked="false"
            aria-label="kaidan ar input"
            tabIndex={0}
          />
        )}
        {odoribaBottom && !kaidan.dr && (
          <div
            style={{
              position: "absolute",
              height: LengthData.OdoribaHeight,
              width: LengthData.Width / 3,
              left: 0,
              top: LengthData.Height,
              zIndex: 200,
              textAlign: "center",
              lineHeight: "3rem",
              backgroundColor: undefined,
              border: undefined,
            }}
            onMouseOver={kaidanMouseOver}
            onFocus={kaidanMouseOver}
            onMouseLeave={kaidanMouseLeave(kaidan.dr)}
            onKeyDown={() => {}}
            onClick={() => {
              kaidan.dr
                ? updateKaidan({ ...kaidan, dr: false })
                : updateKaidan({ ...kaidan, dr: true });
            }}
            role="checkbox"
            aria-checked="false"
            aria-label="kaidan dr input"
            tabIndex={0}
          />
        )}
        {odoribaBottom && !kaidan.dl && (
          <div
            style={{
              position: "absolute",
              height: LengthData.OdoribaHeight,
              width: LengthData.Width / 3,
              left: 0 + (LengthData.Width * 2) / 3,
              top: LengthData.Height,
              zIndex: 200,
              textAlign: "center",
              lineHeight: "3rem",
              backgroundColor: undefined,
              border: undefined,
            }}
            onMouseOver={kaidanMouseOver}
            onFocus={kaidanMouseOver}
            onMouseLeave={kaidanMouseLeave(kaidan.dl)}
            onKeyDown={() => {}}
            onClick={() => {
              kaidan.dl
                ? updateKaidan({ ...kaidan, dl: false })
                : updateKaidan({ ...kaidan, dl: true });
            }}
            role="checkbox"
            aria-checked="false"
            aria-label="kaidan dl input"
            tabIndex={0}
          />
        )}
        {/* kaidan display */}
        {kaidan.al && (
          <div
            style={{
              position: "absolute",
              height: LengthData.OdoribaHeight,
              width: LengthData.Width,
              left: -LengthData.Width,
              top: -LengthData.OdoribaHeight,
              zIndex: 200,
              textAlign: "center",
              lineHeight: "3rem",
              backgroundColor: "yellow",
              border: "1px solid black",
            }}
          >
            階段
          </div>
        )}
        {kaidan.ar && (
          <div
            style={{
              position: "absolute",
              height: LengthData.OdoribaHeight,
              width: LengthData.Width,
              left: LengthData.Width,
              top: -LengthData.OdoribaHeight,
              zIndex: 200,
              textAlign: "center",
              lineHeight: "3rem",
              backgroundColor: "yellow",
              border: "1px solid black",
            }}
          >
            階段
          </div>
        )}
        {kaidan.dr && (
          <div
            style={{
              position: "absolute",
              height: LengthData.OdoribaHeight,
              width: LengthData.Width,
              left: -LengthData.Width,
              top: LengthData.Height,
              zIndex: 200,
              textAlign: "center",
              lineHeight: "3rem",
              backgroundColor: "yellow",
              border: "1px solid black",
            }}
          >
            階段
          </div>
        )}
        {kaidan.dl && (
          <div
            style={{
              position: "absolute",
              height: LengthData.OdoribaHeight,
              width: LengthData.Width,
              left: LengthData.Width,
              top: LengthData.Height,
              zIndex: 200,
              textAlign: "center",
              lineHeight: "3rem",
              backgroundColor: "yellow",
              border: "1px solid black",
            }}
          >
            階段
          </div>
        )}
        {/* odoriba input */}
        <div
          style={{
            position: "absolute",
            height: LengthData.OdoribaHeight,
            width: LengthData.Width,
            left: 0,
            top: -LengthData.OdoribaHeight,
            zIndex: 100,
            textAlign: "center",
            lineHeight: "3rem",
            backgroundColor: odoribaUp ? "white" : undefined,
            border: odoribaUp ? "1px solid black" : undefined,
          }}
          onMouseOver={odoribaMouseOver}
          onFocus={odoribaMouseOver}
          onMouseLeave={odoribaMouseLeave(odoribaUp)}
          onKeyDown={() => {}}
          onClick={() => {
            updateOdoribaUp(!odoribaUp);
          }}
          role="checkbox"
          aria-checked="false"
          aria-label="odoriba up input"
          tabIndex={0}
        >
          {odoribaUp ? "踊り場" : ""}
        </div>
        {index === 0 ? (
          <>
            <div
              style={{
                position: "absolute",
                height: LengthData.Height / 2,
                width: LengthData.OdoribaHeight,
                left: -LengthData.OdoribaHeight,
                top: 0,
                zIndex: 100,
                textAlign: "center",
                lineHeight: "4.3rem",
                backgroundColor: odoribaLeftUp ? "white" : undefined,
                border: odoribaLeftUp ? "1px solid black" : undefined,
              }}
              onMouseOver={odoribaMouseOver}
              onFocus={odoribaMouseOver}
              onMouseLeave={odoribaMouseLeave(odoribaLeftUp)}
              onKeyDown={() => {}}
              onClick={() => updateOdoribaLeftUp(!odoribaLeftUp)}
              role="checkbox"
              aria-checked="false"
              aria-label="odoriba left up input"
              tabIndex={0}
            >
              {odoribaLeftUp ? "踊り場" : ""}
            </div>
            <div
              style={{
                position: "absolute",
                height: LengthData.Height / 2,
                width: LengthData.OdoribaHeight,
                left: -LengthData.OdoribaHeight,
                top: LengthData.Height / 2,
                zIndex: 100,
                textAlign: "center",
                lineHeight: "4.3rem",
                backgroundColor: odoribaLeftBottom ? "white" : undefined,
                border: odoribaLeftBottom ? "1px solid black" : undefined,
              }}
              onMouseOver={odoribaMouseOver}
              onFocus={odoribaMouseOver}
              onMouseLeave={odoribaMouseLeave(odoribaLeftBottom)}
              onKeyDown={() => {}}
              onClick={() => updateOdoribaLeftBottom(!odoribaLeftBottom)}
              role="checkbox"
              aria-checked="false"
              aria-label="odoriba left bottom input"
              tabIndex={0}
            >
              {odoribaLeftBottom ? "踊り場" : ""}
            </div>
          </>
        ) : (
          <></>
        )}
        {index === houseMetadata.rentousu - 1 ? (
          <>
            <div
              style={{
                position: "absolute",
                height: LengthData.Height / 2,
                width: LengthData.OdoribaHeight,
                left: LengthData.Width,
                top: 0,
                zIndex: 100,
                textAlign: "center",
                lineHeight: "4.3rem",
                backgroundColor: odoribaRightUp ? "white" : undefined,
                border: odoribaRightUp ? "1px solid black" : undefined,
              }}
              onMouseOver={odoribaMouseOver}
              onFocus={odoribaMouseOver}
              onMouseLeave={odoribaMouseLeave(odoribaRightUp)}
              onKeyDown={() => {}}
              onClick={() => updateOdoribaRightUp(!odoribaRightUp)}
              role="checkbox"
              aria-checked="false"
              aria-label="odoriba right up input"
              tabIndex={0}
            >
              {odoribaRightUp ? "踊り場" : ""}
            </div>
            <div
              style={{
                position: "absolute",
                height: LengthData.Height / 2,
                width: LengthData.OdoribaHeight,
                left: LengthData.Width,
                top: LengthData.Height / 2,
                zIndex: 100,
                textAlign: "center",
                lineHeight: "4.3rem",
                backgroundColor: odoribaRightBottom ? "white" : undefined,
                border: odoribaRightBottom ? "1px solid black" : undefined,
              }}
              onMouseOver={odoribaMouseOver}
              onFocus={odoribaMouseOver}
              onMouseLeave={odoribaMouseLeave(odoribaRightBottom)}
              onKeyDown={() => {}}
              onClick={() => updateOdoribaRightBottom(!odoribaRightBottom)}
              role="checkbox"
              aria-checked="false"
              aria-label="odoriba right bottom input"
              tabIndex={0}
            >
              {odoribaRightBottom ? "踊り場" : ""}
            </div>
          </>
        ) : (
          <></>
        )}
        <div
          style={{
            position: "absolute",
            height: LengthData.OdoribaHeight,
            width: LengthData.Width,
            left: 0,
            top: LengthData.Height,
            zIndex: 100,
            textAlign: "center",
            lineHeight: "3rem",
            backgroundColor: odoribaBottom ? "white" : undefined,
            border: odoribaBottom ? "1px solid black" : undefined,
          }}
          onMouseOver={odoribaMouseOver}
          onFocus={odoribaMouseOver}
          onMouseLeave={odoribaMouseLeave(odoribaBottom)}
          onKeyDown={() => {}}
          onClick={() => {
            odoribaBottom
              ? updateOdoribaBottom(false)
              : updateOdoribaBottom(true);
          }}
          role="checkbox"
          aria-checked="false"
          aria-label="odoriba bottom input"
          tabIndex={0}
        >
          {odoribaBottom ? "踊り場" : ""}
        </div>
        {/* majikiri input */}
        <div
          style={{
            position: "absolute",
            height: LengthData.Height,
            width: LengthData.MajikiriWidth,
            left: (LengthData.Width - LengthData.MajikiriWidth) / 2,
            top: 0,
            zIndex: 100,
            backgroundColor: majikiriCenter ? "black" : undefined,
          }}
          onMouseOver={majikiriMouseOver}
          onFocus={majikiriMouseOver}
          onMouseLeave={majikiriMouseLeave(majikiriCenter)}
          onKeyDown={() => {}}
          onClick={() => updateMajikiriCenter(!majikiriCenter)}
          role="checkbox"
          aria-checked="false"
          aria-label="majikiri input"
          tabIndex={0}
        />
        {index === houseMetadata.rentousu - 1 ? (
          <></>
        ) : (
          <div
            style={{
              position: "absolute",
              height: LengthData.Height,
              width: LengthData.MajikiriWidth,
              left: LengthData.Width - LengthData.MajikiriWidth / 2,
              top: 0,
              zIndex: 100,
              backgroundColor: majikiriRight ? "black" : undefined,
            }}
            onMouseOver={majikiriMouseOver}
            onFocus={majikiriMouseOver}
            onMouseLeave={majikiriMouseLeave(majikiriRight)}
            onKeyDown={() => {}}
            onClick={() => updateMajikiriRight(!majikiriRight)}
            role="checkbox"
            aria-checked="false"
            aria-label="majikiri input"
            tabIndex={0}
          />
        )}
      </Paper>
      <Grid pt={17}>
        <Select
          defaultValue={
            houseItemCodes[index].type ? houseItemCodes[index].type : ""
          }
          onChange={(
            event: SelectChangeEvent<HTMLInputElement>,
            child: ReactNode
          ) => {
            updateContent({
              ...houseItemCodes[index],
              type: event.target.value,
            });
          }}
          displayEmpty
          label="型番名"
          sx={{ width: LengthData.Width }}
          renderValue={(selected) => {
            if (!selected) {
              return <em>型番名</em>;
            }

            return selected;
          }}
        >
          <MenuItem disabled value="">
            <em>型番名</em>
          </MenuItem>
          {houseType.houseTypes.map((item) => (
            <MenuItem value={item.name} key={item.name}>
              {item.name}
            </MenuItem>
          ))}
        </Select>
      </Grid>
      <Grid>
        <Select
          defaultValue={houseItemCodes[index].code}
          onChange={(e) => {
            updateContent({ ...houseItemCodes[index], code: e.target.value });
          }}
          displayEmpty
          label="製品番号"
          sx={{ width: LengthData.Width }}
          renderValue={(selected) => {
            if (!selected) {
              return <em>製品番号</em>;
            }

            return selected;
          }}
        >
          <MenuItem disabled value="">
            <em>製品番号</em>
          </MenuItem>
          {houseItemCodes[index].type
            ? houseItem.houseItems
                .filter((item) => item.type == houseItemCodes[index].type)
                .map((item) => (
                  <MenuItem value={item.name} key={item.name}>
                    {item.name}
                  </MenuItem>
                ))
            : houseItem.houseItems.map((item) => (
                <MenuItem value={item.name} key={item.name}>
                  {item.name}
                </MenuItem>
              ))}
        </Select>
      </Grid>
    </>
  );
};

export default HousePieceInput;
