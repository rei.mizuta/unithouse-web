import { Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { Dispatch, SetStateAction, useEffect } from "react";
import { HouseRequirement, HouseSetRequirement } from "store/orderReducer";
import HousePieceInput from "./HousePieceInput";

const HouseInput: React.FC<{
  houseRequirement: HouseRequirement;
  updateHouseRequirement: Dispatch<SetStateAction<HouseRequirement>>;
}> = ({ houseRequirement, updateHouseRequirement }) => {
  const { rentousu, kaisu, setNumber, size } = houseRequirement.metadata;

  const rearrange = <T,>(
    arr: Array<T>,
    length: number,
    defaultVal: T
  ): void => {
    if (arr.length > length) arr.splice(length);
    while (arr.length < length)
      arr.push(JSON.parse(JSON.stringify(defaultVal))); // deep copy
  };

  useEffect(() => {
    rearrange(houseRequirement.content, setNumber, {
      OneFloor: [],
      TwoFloor: [],
      majikiriOneFloor: [],
      majikiriTwoFloor: [],
      kaidan: [],
      odoriba: [],
    });
    for (let setIdx = 0; setIdx < setNumber; setIdx++) {
      rearrange(houseRequirement.content[setIdx].OneFloor, rentousu, {
        type: null,
        code: null,
      });
      rearrange(houseRequirement.content[setIdx].TwoFloor, rentousu, {
        type: null,
        code: null,
      });
      rearrange(houseRequirement.content[setIdx].kaidan, rentousu, []);
      rearrange(houseRequirement.content[setIdx].odoriba, rentousu, []);
    }
    updateHouseRequirement({ ...houseRequirement });
  }, [rentousu, kaisu, setNumber]);

  return (
    <>
      {houseRequirement.content.map((houseSetContent, setIdx) => (
        <Grid sx={{ pt: 10, pb: 2 }} item xs={12} key={`set-${setIdx}`}>
          <h2>{setIdx + 1}セット</h2>
          {kaisu === 2 ? (
            [
              <>
                <Typography align="left" variant="h2">
                  2階
                </Typography>
                <Grid
                  container
                  sx={{ pt: 10, pb: 2 }}
                  justifyContent="center"
                  spacing={0}
                >
                  {houseSetContent.TwoFloor.map((value, idx) => (
                    <Box pt={7} pb={7} key={idx}>
                      <Grid key={idx} item>
                        <HousePieceInput
                          houseMetadata={houseRequirement.metadata}
                          setIdx={setIdx}
                          kaisu={2}
                          index={idx}
                          content={houseRequirement.content}
                          updateContent={(content: HouseSetRequirement) =>
                            updateHouseRequirement({
                              ...houseRequirement,
                              content: houseRequirement.content.map(
                                (item, index) =>
                                  index === setIdx ? content : item
                              ),
                            })
                          }
                        />
                      </Grid>
                    </Box>
                  ))}
                </Grid>
              </>,
              <>
                <Typography align="left" variant="h2">
                  1階
                </Typography>
                <Grid
                  container
                  sx={{ pt: 10, pb: 2 }}
                  justifyContent="center"
                  spacing={0}
                >
                  {houseSetContent.OneFloor.map((value, idx) => (
                    <Box pt={7} pb={7} key={idx}>
                      <Grid key={idx} item>
                        <HousePieceInput
                          houseMetadata={houseRequirement.metadata}
                          setIdx={setIdx}
                          kaisu={1}
                          index={idx}
                          content={houseRequirement.content}
                          updateContent={(content: HouseSetRequirement) =>
                            updateHouseRequirement({
                              ...houseRequirement,
                              content: houseRequirement.content.map(
                                (item, index) =>
                                  index === setIdx ? content : item
                              ),
                            })
                          }
                        />
                      </Grid>
                    </Box>
                  ))}
                </Grid>
              </>,
            ]
          ) : (
            <Grid container justifyContent="center" spacing={0}>
              {houseSetContent.OneFloor.map((value, idx) => (
                <Box pt={7} pb={7} key={idx}>
                  <Grid key={idx} item>
                    <HousePieceInput
                      houseMetadata={houseRequirement.metadata}
                      setIdx={setIdx}
                      kaisu={1}
                      index={idx}
                      content={houseRequirement.content}
                      updateContent={(content: HouseSetRequirement) =>
                        updateHouseRequirement({
                          ...houseRequirement,
                          content: houseRequirement.content.map((item, index) =>
                            index === setIdx ? content : item
                          ),
                        })
                      }
                    />
                  </Grid>
                </Box>
              ))}
            </Grid>
          )}
        </Grid>
      ))}
    </>
  );
};

export default HouseInput;
