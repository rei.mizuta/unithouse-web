import { TextField } from "@mui/material";
import { Dispatch, SetStateAction } from "react";
import { HouseRequirement } from "store/orderReducer";

const OrderNumeralInput: React.FC<{
  houseRequirement: HouseRequirement;
  updateHouseRequirement: Dispatch<SetStateAction<HouseRequirement>>;
}> = ({ houseRequirement, updateHouseRequirement }) => {
  const handleUpdate = (event: React.ChangeEvent<HTMLInputElement>) => {
    const numInputValue: number = parseInt(event.target.value);
    if (numInputValue > 0)
      updateHouseRequirement({
        ...houseRequirement,
        metadata: {
          ...houseRequirement.metadata,
          [event.target.id]: numInputValue,
        },
      });
  };
  return (
    <>
      <TextField
        id="rentousu"
        type="number"
        value={houseRequirement.metadata.rentousu}
        inputProps={{ inputMode: "numeric", pattern: "[0-9]*", min: 2 }}
        style={{ width: 80 }}
        onChange={handleUpdate}
      />
      連棟
      <TextField
        id="kaisu"
        type="number"
        value={houseRequirement.metadata.kaisu}
        inputProps={{ inputMode: "numeric", pattern: "[1-2]*", min: 1, max: 2 }}
        style={{ width: 50 }}
        onChange={handleUpdate}
      />
      階
      <TextField
        id="setNumber"
        type="number"
        value={houseRequirement.metadata.setNumber}
        inputProps={{ inputMode: "numeric", pattern: "[0-9]*", min: 1 }}
        style={{ width: 50 }}
        onChange={handleUpdate}
      />
      セット
    </>
  );
};

export default OrderNumeralInput;
