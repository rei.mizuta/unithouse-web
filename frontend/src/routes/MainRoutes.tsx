import { lazy } from "react";
import { HouseSize } from "store/orderReducer";
// project imports
import MainLayout from "layout/MainLayout";
import Loadable from "ui-component/Loadable";

// dashboard routing
const DashboardDefault = Loadable(
  lazy(() => import("views/dashboard/Default"))
);

// utilities routing
const UtilsTypography = Loadable(
  lazy(() => import("views/utilities/Typography"))
);
const UtilsColor = Loadable(lazy(() => import("views/utilities/Color")));
const UtilsShadow = Loadable(lazy(() => import("views/utilities/Shadow")));
const UtilsMaterialIcons = Loadable(
  lazy(() => import("views/utilities/MaterialIcons"))
);
const UtilsTablerIcons = Loadable(
  lazy(() => import("views/utilities/TablerIcons"))
);

// housetypes routing
const HouseTypeView = Loadable(lazy(() => import("views/pages/HouseTypes")));
const HouseTypeCreateView = Loadable(
  lazy(() => import("views/pages/HouseTypes/create"))
);
// houseitems routing
const HouseItemView = Loadable(lazy(() => import("views/pages/HouseItems")));
const HouseItemCreateView = Loadable(
  lazy(() => import("views/pages/HouseItems/create"))
);
const HouseItemUpdateView = Loadable(
  lazy(() => import("views/pages/HouseItems/update"))
);
// order routing
const OrderView = Loadable(lazy(() => import("views/pages/Orders")));
const OrderCreateView = Loadable(
  lazy(() => import("views/pages/Orders/create"))
);
const OrderUpdateView = Loadable(
  lazy(() => import("views/pages/Orders/update"))
);

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
  path: "/",
  element: <MainLayout />,
  children: [
    {
      path: "/",
      element: <DashboardDefault />,
    },
    {
      path: "/dashboard/default",
      element: <DashboardDefault />,
    },
    {
      path: "/utils/util-typography",
      element: <UtilsTypography />,
    },
    {
      path: "/utils/util-color",
      element: <UtilsColor />,
    },
    {
      path: "/utils/util-shadow",
      element: <UtilsShadow />,
    },
    {
      path: "/icons/tabler-icons",
      element: <UtilsTablerIcons />,
    },
    {
      path: "/icons/material-icons",
      element: <UtilsMaterialIcons />,
    },
    {
      path: "/house-types",
      element: <HouseTypeView />,
    },
    {
      path: "/house-types/create",
      element: <HouseTypeCreateView />,
    },
    {
      path: "/house-items",
      element: <HouseItemView />,
    },
    {
      path: "/house-items/create",
      element: <HouseItemCreateView />,
    },
    {
      path: "/house-items/update/:id",
      element: <HouseItemUpdateView />,
    },
    {
      path: "/order/size4",
      element: <OrderView size={HouseSize.Four} />,
    },
    {
      path: "/order/size5",
      element: <OrderView size={HouseSize.Five} />,
    },
    {
      path: "/order/size4/create",
      element: <OrderCreateView size={HouseSize.Four} />,
    },
    {
      path: "/order/size5/create",
      element: <OrderCreateView size={HouseSize.Five} />,
    },
    {
      path: "/order/size4/update/:id",
      element: <OrderUpdateView size={HouseSize.Four} />,
    },
    {
      path: "/order/size5/update/:id",
      element: <OrderUpdateView size={HouseSize.Five} />,
    },
  ],
};

export default MainRoutes;
